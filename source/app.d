import std.stdio;
import std.algorithm;

import strategies;
import combinators;

void main()
{
    auto stam = arrays(arrays(floats(0.5, 3.5), 2, 10), 2, 5).map!((float[][] a) => a.map!(x => x.map!(x => x + 100)));
    writeln(stam.next());
    auto s = ints(5, 6).flatmap!((int l) => arrays(ints(), l, l));
    writeln(s.next());
}
