import std.algorithm;
import std.array;

import impl;
import meta;

auto ints(int minValue = int.min, int maxValue = int.max)
{
    return new GenInt(minValue, maxValue);
}

auto floats(float minValue = float.min_normal, float maxValue = float.max)
{
    return new GenFloat(minValue, maxValue);
}

auto arrays(S)(S st, int minSize, int maxSize)
{
    return new GenArray!S(st, minSize, maxSize);
}
