import impl;

auto minimize(GenInt st)
{
    return [
        new GenInt(st.minValue + (st.maxValue - st.minValue) / 2, st.maxValue),
        new GenInt(st.minValue, st.maxValue / 2)
    ];
}

auto minimize(S)(GenArray!S st)
{
    return [
        new GenArray!S(st.baseGen, st.minSize + (st.maxSize - st.minSize) / 2, st.maxSize),
        new GenArray!S(st.baseGen, st.minSize, st.maxSize / 2)
    ];
}

auto minimize(S, U)(GenMapped!(S, U) st)
{
    return st.baseGen.minimize.map!(s => new GenMapped!(S, U)(s, st.f)).array();
}
