import std.traits;

template DefaultInit(T)
{
    enum T DefaultInit = T.init;
}

alias RetTy(alias func) = ReturnType!(FunctionTypeOf!func);
