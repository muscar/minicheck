import std.traits;

import impl;
import meta;

template map(alias func)
    if (isCallable!func)
{
    auto map(S)(S st)
        if (is(S : Gen!S.t))
    {
        return new GenMapped!(S, func)(st);
    }
}

template flatmap(alias func)
    if (isCallable!func)
{
    auto flatmap(S)(S st)
        if (is(S : Gen!S.t))
    {
        return new GenMapped!(S, (S.t x) => func(x).next())(st);
    }
}
