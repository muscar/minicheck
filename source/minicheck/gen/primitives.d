import std.random;
import std.traits;

T genNumeric(T)(T minValue, T maxValue)
    if (isNumeric!T)
{
    return uniform(minValue, maxValue);
}
