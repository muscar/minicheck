import std.array;
import std.meta;
import std.random;
import std.traits;
import std.typecons;

import meta;
import primitives;

abstract class Gen(T)
{
    alias t = T;

    T next();
}

class GenPrimitive(alias func) : Gen!(RetTy!func)
    if (isCallable!func)
{
    alias Names = ParameterIdentifierTuple!func;
    alias Params = Parameters!func;

    Params params;
    auto paramMap = tuple!(Names)(staticMap!(DefaultInit, Params));

    alias paramMap this;

    this(Parameters!func params)
    {
        this.params = params;
        this.paramMap = tuple!(Names)(params);
    }

    override RetTy!func next()
    {
        return func(params);
    }
}

class GenArray(S) : Gen!(S.t[])
    if (is(S : Gen!S.t))
{
    S baseGen;
    int minSize;
    int maxSize;

    this(S baseGen, int minSize, int maxSize)
    {
        this.baseGen = baseGen;
        this.minSize = minSize;
        this.maxSize = maxSize;
    }

    override S.t[] next()
    {
        auto acc = appender!(S.t[])();
        for (int i = 0; i < uniform(minSize, maxSize + 1); ++i) {
            acc.put(baseGen.next());
        }
        return acc.data();
    }
}

class GenMapped(S, alias func) : Gen!(RetTy!func)
    if (isCallable!func && is(S : Gen!S.t))
{
    S baseGen;

    this(S baseGen)
    {
        this.baseGen = baseGen;
    }

    override RetTy!func next()
    {
        return func(baseGen.next());
    }
}

alias GenInt = GenPrimitive!(genNumeric!int);

alias GenFloat = GenPrimitive!(genNumeric!float);
